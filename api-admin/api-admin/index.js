
var DB = require('./fn/event-handlers');
var exe_query = new DB();


module.exports = async function (context, req) {
    
    var respond = (response) => { context.res = response; };

    // GET Requests
    if(req.query.get) {
        switch (req.query.get) {
            case 'list-of-organisations': 
                await exe_query.getOrganisations().then(respond); 
                break;
            
            case 'list-of-users': 
                await exe_query.getAllFrom('users').then(respond); 
                break;

            default: context.res = {status: 400, body: 'Please provide a valid endpoint.'}
        }
    }


    // POST Requests
    if(req.body && req.body.post && req.body.data) {

        switch (req.body.post) {
            case 'new-organisation': 
                await exe_query.addOrganisation(req.body.data).then(exe_query.getOrganisations).then(respond);
                break;

            case 'new-user': 
                await exe_query.addUser(req.body.data).then(exe_query.getUsers).then(respond);
                break;
                
            case 'delete-organisation': 
                await exe_query.deleteOrganisation(req.body.data).then(exe_query.getOrganisations).then(respond);
                break;

            case 'delete-user': 
                await exe_query.deleteUser(req.body.data).then(exe_query.getUsers).then(respond);
                break;

            default: context.res = {status: 400, body: 'Please provide a valid endpoint.'}
        }
    }
};