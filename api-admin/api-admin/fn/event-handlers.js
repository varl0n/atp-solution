const db = require('./database-helpers');
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var env = require('./env.production');


module.exports = (function(){
    
    var connection = null;

    var EventHandler = function(){
        connection = new Connection(env.database);
    };



    /* 
     * Add new organisation then select all organisations from the DB and send them back
     */    
    EventHandler.prototype.addOrganisation = (org) => {
        return new Promise((resolve) => {
            
            org.authKey = db.objectToMD5(org);
            
            let posted = db.objectPropToSQL(org);

            request = new Request(
                'INSERT INTO organisations (' + posted.columns + ') VALUES (' + posted.values + ');',
                function(err, rowCount, rows) {
                    err && resolve({ status: 500, body: err });
                });

            request.on('error', function(err) {
                resolve({ status: 500, body: err });
            })

            request.on('requestCompleted', function(){
                resolve();
            })

            // Execute SQL statement
            connection.execSql(request);

        });
    };



    /* 
     * Add new User then select all users from the DB and send them back
     */    
    EventHandler.prototype.addUser = (user) => {
        return new Promise((resolve) => {
            
            let posted = db.objectPropToSQL(user);

            request = new Request(
                'INSERT INTO users (' + posted.columns + ') VALUES (' + posted.values + ');',
                function(err, rowCount, rows) {
                    err && resolve({ status: 500, body: err });
                });

            request.on('error', function(err) {
                resolve({ status: 500, body: err });
            })

            request.on('requestCompleted', function(){
                resolve();
            })

            // Execute SQL statement
            connection.execSql(request);

        });
    };



    
    EventHandler.prototype.getOrganisations = () => {
        return new Promise((resolve) => {
            var query = "SELECT * FROM organisations WHERE status <> 0 ORDER BY id desc";
            var response = {status: 200, body: null};
            var result = [];

            var request = new Request(query, function(err) {
                err && resolve({ status: 500, body: err });
            });            

            request.on('row', function(columns) {
                var row = {};
                columns.forEach(function(column) {
                    row[column.metadata.colName] = column.value;
                });
                result.push(row);
            });

            request.on('doneProc', function(rowCount, more, rows) {
                if(!more){
                    resolve({
                        status: 200,
                        body: {error: false, rows: result}
                    });
                }
            });

            connection.execSql(request);
        });
    };



    EventHandler.prototype.getUsers = () => {
        return new Promise((resolve) => {
            var query = `SELECT * FROM users WHERE status <> 0 ORDER BY id desc`;
            var response = {status: 200, body: null};
            var result = [];

            var request = new Request(query, function(err) {
                err && resolve({ status: 500, body: err });
            });            

            request.on('row', function(columns) {
                var row = {};
                columns.forEach(function(column) {
                    row[column.metadata.colName] = column.value;
                });
                result.push(row);
            });

            request.on('doneProc', function(rowCount, more, rows) {
                if(!more){
                    resolve({
                        status: 200,
                        body: {error: false, rows: result}
                    });
                }
            });

            connection.execSql(request);
        });
    };



    EventHandler.prototype.getAllFrom = (table) => {
        return new Promise((resolve) => {
            var query = 'SELECT * FROM ' + table + ' WHERE status <> 0 ORDER BY id desc';
            var response = {status: 200, body: null};
            var result = [];

            var request = new Request(query, function(err) {
                err && resolve({ status: 500, body: err });
            });            

            request.on('row', function(columns) {
                var row = {};
                columns.forEach(function(column) {
                    row[column.metadata.colName] = column.value;
                });
                result.push(row);
            });

            request.on('doneProc', function(rowCount, more, rows) {
                if(!more){
                    resolve({
                        status: 200,
                        body: {error: false, rows: result}
                    });
                }
            });

            connection.execSql(request);
        });
    };





    EventHandler.prototype.deleteOrganisation = (org) => {
        return new Promise((resolve) => {
            request = new Request(
                `UPDATE organisations SET status = 0 WHERE id = ${org.id};`,
                function(err) {
                    err && resolve({ status: 500, body: err });
                });

            request.on('error', function(err) {
                err && resolve({ status: 500, body: err });
            });

            request.on('requestCompleted', function(){
                resolve();
            });

            connection.execSql(request);
        });
    };




    EventHandler.prototype.deleteUser = (user) => {
        return new Promise((resolve) => {
            request = new Request(
                `UPDATE users SET status = 0 WHERE id = ${user.id};`,
                function(err) {
                    err && resolve({ status: 500, body: err });
                });

            request.on('error', function(err) {
                err && resolve({ status: 500, body: err });
            });

            request.on('requestCompleted', function(){
                resolve();
            });

            connection.execSql(request);
        });
    };


    return EventHandler;

})();