package assessment_form;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;


public class Controller {

    @FXML
    public TextField nameField;
    @FXML
    public TextField dobField;
    @FXML
    public TextField emailField;
    @FXML
    public TextField addr1Field;
    @FXML
    public TextField addr2Field;
    @FXML
    public TextField cityField;
    @FXML
    public TextField eirCodeField;
    @FXML
    public TextField countyField;
    @FXML
    public TextArea descriptField;
    @FXML
    public ImageView imgWebCamCapturedImage;
    @FXML
    public BorderPane bpWebCamPaneHolder;


    public PatientDAO newPatient;


    @FXML
    public void submitFormNewPatient() {
        System.out.println("Adding new Patient to the system");

        this.newPatient = new PatientDAO();

        this.newPatient.setName(nameField.getText());
        this.newPatient.setDob(this.dobField.getText());
        this.newPatient.setEmail(this.emailField.getText());
        this.newPatient.setAddr1(this.addr1Field.getText());
        this.newPatient.setAddr2(this.addr2Field.getText());
        this.newPatient.setCity(this.cityField.getText());
        this.newPatient.setEirCode(this.eirCodeField.getText());
        this.newPatient.setCounty(this.countyField.getText());
        this.newPatient.setDescription(this.descriptField.getText());

        System.out.println(this.newPatient.getName());
        System.out.println(this.newPatient.getDob());
        System.out.println(this.newPatient.getEmail());
        System.out.println(this.newPatient.getAddr1());
        System.out.println(this.newPatient.getAddr2());
        System.out.println(this.newPatient.getCity());
        System.out.println(this.newPatient.getEirCode());
        System.out.println(this.newPatient.getCounty());
        System.out.println(this.newPatient.getDescription());
    }


    public void activateCam () {

    }
}
