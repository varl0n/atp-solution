package assessment_form;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("main_layout.fxml"));
        Scene newPatientScene = new Scene(root, 700, 400);
        newPatientScene.getStylesheets().add("style.css");
        primaryStage.setTitle("ATP Assessment");
        primaryStage.setScene(newPatientScene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

