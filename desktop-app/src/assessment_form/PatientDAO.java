package assessment_form;

public class PatientDAO {
    private String name;
    private String dob;
    private String email;
    private String addr1;
    private String addr2;
    private String city;
    private String eirCode;
    private String county;
    private String description;



    public void setName(String name) {
        this.name = name;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setEirCode(String eirCode) {
        this.eirCode = eirCode;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public void setDescription(String description) {
        this.description = description;
    }




    public String getName() {
        return name;
    }

    public String getDob() {
        return dob;
    }

    public String getEmail() {
        return email;
    }

    public String getAddr1() {
        return addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public String getCity() {
        return city;
    }

    public String getEirCode() {
        return eirCode;
    }

    public String getCounty() {
        return county;
    }

    public String getDescription() {
        return description;
    }
}
