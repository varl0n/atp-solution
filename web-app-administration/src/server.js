const express = require('express');
const path = require('path');
const api = require('./api/endpoints');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var publicweb = process.env.publicweb || './';

app.use(express.static(publicweb));
app.use('/api', api);

console.log('Serving');

app.get('*', (req, res) => {
  res.sendFile(`index.html`, { root: publicweb });
});

const port = process.env.PORT || '3000';
app.listen(port, () => console.log(`API running on ${process.env.publicweb}:${port}`));
