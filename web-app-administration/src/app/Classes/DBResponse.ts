export class DBResponse<T> {
    public error: string;
    public rows: Array<T>;
    public fields: any;
}