import { Observer } from './Observer';

export interface Subscriber {
	notify();
	subscribe(observer: Observer);
}
