export class UserDAO {

	private id: number;
	private organisation_id: number;
	private name: string;
	private position: string;
	private status: number;
	private email: string;
	private password: string;
	private timestamp: string;


	public setId(id: number){
		this.id = id;
		return this;
	}
	public setOrganisationId(organisation_id: number){
		this.organisation_id = organisation_id;
		return this;
	}
	public setName(name: string){
		this.name = name;
		return this;
	}
	public setPosition(position: string){
		this.position = position;
		return this;
	}
	public setStatus(status: number){
		this.status = status;
		return this;
	}
	public setEmail(email: string){
		this.email = email;
		return this;
	}
	public setPassword(password: string){
		this.password = password;
		return this;
	}
	public setTimestamp(timestamp: string){
		this.timestamp = timestamp;
		return this;
	}


	public getId(): number{
		return this.id;
	}
	public getOrganisation_id(): number{
		return this.organisation_id;
	}
	public getName(): string{
		return this.name;
	}
	public getPosition(): string{
		return this.position;
	}
	public getStatus(): number{
		return this.status;
	}
	public getEmail(): string{
		return this.email;
	}
	public getPassword(): string{
		return this.password;
	}
	public getTimestamp(): string{
		return this.timestamp;
	}

}