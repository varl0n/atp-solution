
class Endpoints {
    
    private fnKey: string;
    private host: string;

    constructor(host: string){
        this.host = host;
        this.list_of_organisations = host+'?get=list-of-organisations';
        this.list_of_users = host+'?get=list-of-users';
        this.new_organisation = host;
        this.add_new_user = host;
        this.delete_organisation = host;
        this.delete_user = host;
    }

    public new_organisation: string;
    public add_new_user: string;
    public list_of_organisations: string;
    public list_of_users: string;
    public delete_organisation: string;
    public delete_user: string;
}



export class Api {
    public endpoint: Endpoints;
    public fnKey: string = 'IXeP0x12J7/CQbyjIPAIAJWoIhYz3rq6Q0JJzS7oN8PIeU8IBTz3NA==';
    public host: string = 'https://atpadmin-api.azurewebsites.net/api/api-admin';
    // public host: string = 'http://localhost:7071/api/api-admin';

    constructor(){
        this.endpoint = new Endpoints(this.host);
    }
}
