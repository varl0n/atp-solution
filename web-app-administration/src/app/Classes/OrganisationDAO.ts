export class OrganisationDAO {

    constructor() {}

    private id: number;
    private name: string;
    private status: number;
    private authKey: string;
    private primaryContactId: number;
    private address1: string;
    private address2: string;
    private city: string;
    private email: string;
    private phone: string;
    private sector: string;
    private timestamp: string;


    public setOrganisationID(id: number) {
        this.id = id;
    }
    
    public setName(name: string) {
        this.name = name;
    }

    public setStatus(status: number) {
        this.status = status;
    }

    public setPrimaryContactId(primaryContactId: number) {
        this.primaryContactId = primaryContactId;
    }

    public setAddress1(address1: string) {
        this.address1 = address1;
    }

    public setAddress2(address2: string) {
        this.address2 = address2;
    }

    public setCity(city: string) {
        this.city = city;
    }

    public setEmail(email: string) {
        this.email = email;
    }

    public setPhone(phone: string) {
        this.phone = phone;
    }

    public setSector(sector: string) {
        this.sector = sector;
    }

    public setAuthKey(authKey: string) {
        this.authKey = authKey;
    }


    


    public getOrganisationID(): number {
        return this.id;
    }
    public getName(): string {
        return this.name;
    }
    public getStatus(): number {
        return this.status;
    }
    public getPrimaryContactId(): number {
        return this.primaryContactId;
    }
    public getAddress1(): string {
        return this.address1;
    }
    public getAddress2(): string {
        return this.address2;
    }
    public getCity(): string {
        return this.city;
    }
    public getEmail(): string {
        return this.email;
    }
    public getPhone(): string {
        return this.phone;
    }
    public getSector(): string {
        return this.sector;
    }
    public getAuthKey(): string {
        return this.authKey;
    }
    public getTimestamp(): string {
        return this.timestamp;
    }
}
