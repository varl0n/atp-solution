import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewManageOrganisationsComponent } from './Views/view-manage-organisations/view-manage-organisations.component';
import { ViewManageUsersComponent } from './Views/view-manage-users/view-manage-users.component';

const routes: Routes = [
  {path: 'manage-organisations', component: ViewManageOrganisationsComponent},
  {path: 'manage-users', component: ViewManageUsersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
