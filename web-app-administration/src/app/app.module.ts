import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DatabaseService } from "./Services/database/database.service";
import { RepoOrganisations } from "./Services/repo-organisations/repo-organisations.service";
import { ServerService } from "./Services/server/server.service";

import { ViewManageOrganisationsComponent } from './Views/view-manage-organisations/view-manage-organisations.component';
import { ViewManageUsersComponent } from './Views/view-manage-users/view-manage-users.component';
import { FormNewOrganisationComponent } from "./Components/form-new-organisation/form-new-organisation.component";
import { OrganisationsListManagedComponent } from './Components/organisations-list-managed/organisations-list-managed.component';
import { FormNewUserComponent } from './Components/form-new-user/form-new-user.component';
import { UsersListManagedComponent } from './Components/users-list-managed/users-list-managed.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewManageOrganisationsComponent,
    FormNewOrganisationComponent,
    OrganisationsListManagedComponent,
    ViewManageUsersComponent,
    FormNewUserComponent,
    UsersListManagedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    DatabaseService,
    RepoOrganisations,
    ServerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
