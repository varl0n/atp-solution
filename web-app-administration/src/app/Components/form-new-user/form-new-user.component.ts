import { Component, OnInit } from '@angular/core';
import { DatabaseService } from "../../Services/database/database.service";
import { RepoUsers, UserInfo } from "../../Services/repo-users/repo-users.service";
import { UserDAO } from 'src/app/Classes/UserDAO';
import { RepoOrganisations } from 'src/app/Services/repo-organisations/repo-organisations.service';
import { OrganisationDAO } from 'src/app/Classes/OrganisationDAO';
import { Observer } from 'src/app/Classes/Observer';

class Options {
  public value: any;
  public Name: String;
}

@Component({
  selector: 'form-new-user',
  templateUrl: './form-new-user.component.html',
  styleUrls: ['./form-new-user.component.scss']
})
export class FormNewUserComponent implements OnInit, Observer {

  public fields: UserInfo;
  public orgOptions: Array<Options>;
  public selectedOrg: number;

  constructor(public db: DatabaseService, public orgsRepo: RepoOrganisations) { 
    this.fields = new UserInfo();
    this.orgOptions = new Array<Options>();
    this.orgsRepo.subscribe(this);

    this.update();
  }

  ngOnInit() {

  }

  update() {
    this.orgsRepo.organisationsList.forEach((orgs)=>{
      let opt: Options = new Options();
      opt.value = orgs.getOrganisationID();
      opt.Name = orgs.getName();
      this.orgOptions.push(opt);
    });
  }

  public submitForm() {
    let userDAO = new UserDAO();

    userDAO.setName(this.fields.name);
    userDAO.setEmail(this.fields.email);
    userDAO.setPosition(this.fields.position);
    userDAO.setOrganisationId(this.selectedOrg);

    this.db.addUser(userDAO);
  }

}
