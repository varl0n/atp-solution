import { Component, OnInit } from '@angular/core';
import { RepoUsers, UserInfo } from "../../Services/repo-users/repo-users.service";
import { RepoOrganisations } from '../../Services/repo-organisations/repo-organisations.service';
import { Observer } from "../../Classes/Observer";
import { DatabaseService } from '../../Services/database/database.service';
import { OrganisationDAO } from 'src/app/Classes/OrganisationDAO';


@Component({
  selector: 'users-list-managed',
  templateUrl: './users-list-managed.component.html',
  styleUrls: ['./users-list-managed.component.scss']
})
export class UsersListManagedComponent implements OnInit, Observer {

  public fields: Array<UserInfo>;

  constructor(
    private repoUsers: RepoUsers,
    private repoOrganisations: RepoOrganisations,
    public db: DatabaseService
  ) { 

    this.fields = new Array<UserInfo>();
    this.repoUsers.subscribe(this);
  }

  ngOnInit() {
    this.update();
  }

  update() {
    this.fields = new Array<UserInfo>();
    this.repoUsers.getUsersList().forEach(userDAO => {
      let info = new UserInfo();
      
      let userOrganisationDAO: OrganisationDAO = this.repoOrganisations
                                                        .getOrganisationBy('id', userDAO.getOrganisation_id());

      info.id = userDAO.getId();
      info.name = userDAO.getName();
      info.organisation = userOrganisationDAO ? userOrganisationDAO.getName() : 'N/A';
      info.position = userDAO.getPosition();
      info.status = userDAO.getStatus();
      info.email = userDAO.getEmail();

      this.fields.push(info);
    });
  }

}
