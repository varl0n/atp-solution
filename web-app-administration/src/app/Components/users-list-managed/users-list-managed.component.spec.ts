import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersListManagedComponent } from './users-list-managed.component';

describe('UsersListManagedComponent', () => {
  let component: UsersListManagedComponent;
  let fixture: ComponentFixture<UsersListManagedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersListManagedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListManagedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
