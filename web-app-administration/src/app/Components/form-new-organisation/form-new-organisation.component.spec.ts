import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNewOrganisationComponent } from './form-new-organisation.component';

describe('FormNewOrganisationComponent', () => {
  let component: FormNewOrganisationComponent;
  let fixture: ComponentFixture<FormNewOrganisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNewOrganisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNewOrganisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
