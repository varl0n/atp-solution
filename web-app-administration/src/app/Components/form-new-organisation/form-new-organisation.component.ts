import { Component, OnInit } from '@angular/core';
import { DatabaseService } from "../../Services/database/database.service";
import { OrganisationDAO } from "../../Classes/OrganisationDAO";

@Component({
  selector: 'form-new-organisation',
  templateUrl: './form-new-organisation.component.html',
  styleUrls: ['./form-new-organisation.component.scss']
})
export class FormNewOrganisationComponent implements OnInit {
  public name: string;
  public address1: string;
  public address2: string;
  public city: string;
  public country: string;
  public email: string;
  public phone: string;
  public sector: string;
  public authKey: string;

  constructor(private db: DatabaseService) { }

  ngOnInit() {

  }

  public submitForm() {
    var organisation = new OrganisationDAO();
    
    organisation.setName(this.name);
    organisation.setAddress1(this.address1);
    organisation.setAddress2(this.address2);
    organisation.setCity(this.city);
    organisation.setEmail(this.email);
    organisation.setPhone(this.phone);
    organisation.setSector(this.sector);

    this.db.addOrganisation(organisation);
  }

}
