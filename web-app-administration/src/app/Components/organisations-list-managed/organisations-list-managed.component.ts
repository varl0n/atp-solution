import { Component, OnInit } from '@angular/core';
import { RepoOrganisations, OrganisationInfo } from "../../Services/repo-organisations/repo-organisations.service";
import { Observer } from "../../Classes/Observer";
import { DatabaseService } from '../../Services/database/database.service';


@Component({
  selector: 'organisations-list-managed',
  templateUrl: './organisations-list-managed.component.html',
  styleUrls: ['./organisations-list-managed.component.scss']
})
export class OrganisationsListManagedComponent implements OnInit, Observer {

  public fields: Array<OrganisationInfo>;

  constructor(
    public repoOrganisations: RepoOrganisations, 
    public db: DatabaseService
  ) { 

    this.fields = new Array<OrganisationInfo>();
    this.repoOrganisations.subscribe(this);
  }



  ngOnInit() {
    this.update();
  }

  

  update() {
    this.fields = new Array<OrganisationInfo>();
    this.repoOrganisations.organisationsList.forEach(info => {
      var org_info = new OrganisationInfo;

      org_info.id = info.getOrganisationID();
      org_info.name = info.getName();
      org_info.email = info.getEmail();
      org_info.phone = info.getPhone();
      org_info.address1 = info.getAddress1();
      org_info.authKey = info.getAuthKey();
      // org_info.primaryContact = info.getPrimaryContactId(); 
      org_info.sector = info.getSector();

      this.fields.push(org_info);
    });
  }

}
