import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationsListManagedComponent } from './organisations-list-managed.component';

describe('OrganisationsListManagedComponent', () => {
  let component: OrganisationsListManagedComponent;
  let fixture: ComponentFixture<OrganisationsListManagedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationsListManagedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationsListManagedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
