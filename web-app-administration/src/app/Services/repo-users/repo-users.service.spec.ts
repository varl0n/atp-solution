import { TestBed } from '@angular/core/testing';

import { RepoUsersService } from './repo-users.service';

describe('RepoUsersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RepoUsersService = TestBed.get(RepoUsersService);
    expect(service).toBeTruthy();
  });
});
