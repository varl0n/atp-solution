import { Injectable } from '@angular/core';
import { UserDAO } from '../../Classes/UserDAO';
import { DBResponse } from '../../Classes/DBResponse';
import { Observer } from "../../Classes/Observer";
import { Subscriber } from "../../Classes/Subscriber";


export class UserInfo {
	public id: number;
  public organisation_id: number;
  public organisation: string;
	public name: string;
	public position: string;
	public status: number;
	public email: string;
	public timestamp: string;
}


@Injectable({
  providedIn: 'root'
})
export class RepoUsers implements Subscriber {

  private usersList: Array<UserDAO>;
  private observers: Array<Observer>;

  constructor() {
    this.usersList = new Array<UserDAO>();
    this.observers = new Array<Observer>();
  }


  public updateUsersList = (response: DBResponse<UserDAO>) => {
    this.usersList = new Array<UserDAO>();
    this.makeUsersDAOList(response.rows);
    this.notify();
  }
  

  public getUsersList() {
    return this.usersList;
  }


  public getUserBy(field:string, val:any): UserDAO {
    this.usersList.forEach((userDAO)=>{
      if(userDAO[field] == val) {
        return userDAO;
      }
    });

    return null;
  }


  public makeTestRepo() {
    let response = new DBResponse<UserDAO>();

    let user1 = new UserDAO().setId(1).setName('Dmitry').setEmail('admin@xwerx.com').setOrganisationId(1)
                             .setPosition('Developer').setStatus(1);
    let user2 = new UserDAO().setId(1).setName('John').setEmail('user@titanic.org').setOrganisationId(2)
                             .setPosition('Receptionist').setStatus(1);
    let user3 = new UserDAO().setId(1).setName('Mary').setEmail('user2@titanic.org').setOrganisationId(2)
                             .setPosition('Manager').setStatus(1);
    let user4 = new UserDAO().setId(1).setName('Gary').setEmail('user@apollo.org').setOrganisationId(3)
                             .setPosition('Manager').setStatus(1);
    let user5 = new UserDAO().setId(1).setName('Julia').setEmail('user@apollo.org').setOrganisationId(3)
                             .setPosition('Accountant').setStatus(1);

    response.rows = [user1, user2, user3, user4, user5];

    this.updateUsersList(response);
  }


  public subscribe(observer: Observer) {
    this.observers.push(observer);
  }

  public notify() {
    this.observers.forEach((observer)=>{
      observer.update();
    });
  }


  private makeUsersDAOList(usersData: Array<any>) {
    for(let n in usersData) {
      let userDAO = new UserDAO;

      usersData[n].name && userDAO.setName(usersData[n].name);
      usersData[n].id && userDAO.setId(usersData[n].id);
      usersData[n].organisation_id && userDAO.setOrganisationId(usersData[n].organisation_id);
      usersData[n].name && userDAO.setName(usersData[n].name);
      usersData[n].position && userDAO.setPosition(usersData[n].position);
      usersData[n].status && userDAO.setStatus(usersData[n].status);
      usersData[n].email && userDAO.setEmail(usersData[n].email);
      usersData[n].password && userDAO.setPassword(usersData[n].password);
      usersData[n].timestamp && userDAO.setTimestamp(usersData[n].timestamp);

      this.usersList.push(userDAO);
    }
  }
  

}
