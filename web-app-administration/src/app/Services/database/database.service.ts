import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Api } from "../../Classes/api";
import { DBResponse } from '../../Classes/DBResponse';

import { OrganisationDAO } from "../../Classes/OrganisationDAO";
import { UserDAO } from '../../Classes/UserDAO';
import { RepoOrganisations, OrganisationInfo } from '../../Services/repo-organisations/repo-organisations.service';
import { RepoUsers, UserInfo } from '../../Services/repo-users/repo-users.service';


@Injectable({
    providedIn: 'root'
})

export class DatabaseService {

    public api: Api;
    public requestOptions: Object;

    constructor(
        private http: HttpClient, 
        public repoOrganisations: RepoOrganisations,
        public repoUsers: RepoUsers
    ) { 
        this.api = new Api();
        this.requestOptions = { 
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-functions-key': this.api.fnKey
            })};
        
        this.getContent();

        /* this.repoOrganisations.makeTestRepo();
        this.repoUsers.makeTestRepo(); */

        
    }


    public getContent() {
        this.getListOfOrganisations()
            .then(this.repoOrganisations.updateOrganisationsList);

        this.getListOfUsers()
            .then(this.repoUsers.updateUsersList);
    }


    public getListOfOrganisations() {
        return new Promise((resolve) => {
            this.http.get<DBResponse<OrganisationDAO>>(this.api.endpoint.list_of_organisations, this.requestOptions)
                .subscribe(resolve);
        });
    }


    public getListOfUsers() {
        return new Promise((resolve) => {
            this.http.get<DBResponse<UserDAO>>(this.api.endpoint.list_of_users, this.requestOptions)
            .subscribe(resolve);
        });
    }



    /**
    * addOrganisation
    */
    public addOrganisation(organisation: OrganisationDAO) {
        this.http.post<DBResponse<OrganisationDAO>>( this.api.endpoint.new_organisation, {post: 'new-organisation', data: organisation}, this.requestOptions)
            .subscribe(
                (response) => {
                    this.repoOrganisations.updateOrganisationsList(response);
                },
                (error) => {
                    console.error("Error:", error);
                },

                () => {
                }
            );
    }


    

   public addUser(userDAO: UserDAO) {
        console.log(userDAO);
        this.http.post<DBResponse<UserDAO>>( this.api.endpoint.add_new_user, {post: 'new-user', data: userDAO}, this.requestOptions)
            .subscribe(
                (response) => {
                    this.repoUsers.updateUsersList(response);
                },
                (error) => {
                    console.error("Error:", error);
                },

                () => {
                    this.getListOfUsers();
                }
            );
    }



    public deleteOrganisation(organisation: OrganisationInfo) {
        this.http.post<DBResponse<OrganisationDAO>>( this.api.endpoint.delete_organisation, {post: 'delete-organisation', data: organisation}, this.requestOptions)
            .subscribe(
                (response) => {
                    this.repoOrganisations.updateOrganisationsList(response);
                },
                (error) => {
                    console.error("Error:", error);
                },

                () => {
                }
            );
    }




    public deleteUser(user: UserInfo) {
        this.http.post<DBResponse<UserDAO>>( this.api.endpoint.delete_user, {post: 'delete-user', data: user}, this.requestOptions)
            .subscribe(
                (response) => {
                    this.repoUsers.updateUsersList(response);
                },
                (error) => {
                    console.error("Error:", error);
                },

                () => {
                }
            );
    }
}