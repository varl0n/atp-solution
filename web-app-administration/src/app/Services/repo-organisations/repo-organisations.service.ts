import { Injectable } from '@angular/core';
import { OrganisationDAO } from '../../Classes/OrganisationDAO';
import { DBResponse } from '../../Classes/DBResponse'
import { Observer } from "../../Classes/Observer";
import { Subscriber } from "../../Classes/Subscriber";

export class OrganisationInfo {
  public id: number;
  public name: string;
  public primaryContact: string;
  public address1: string;
  public phone: string;
  public email: string;
  public sector: string;
  public authKey: string;
}



@Injectable({
  providedIn: 'root'
})
export class RepoOrganisations implements Subscriber {

  public organisationsList: Array<OrganisationDAO>;
  private observers: Array<Observer>;

  constructor() { 
    this.organisationsList = new Array<OrganisationDAO>();
    this.observers = new Array<Observer>();
  }


	public updateOrganisationsList = (response: DBResponse<OrganisationDAO>) => {
    this.organisationsList = new Array<OrganisationDAO>();
    // this.notify();
    response.rows && this.makeDAOList(response.rows);
    this.notify();
  }


  public getOrganisationsList = () => {
    return this.organisationsList;
  }


  public getOrganisationBy(field:string, val:any): OrganisationDAO {
    for(var o in this.organisationsList){
      if(this.organisationsList[o][field] === val) {
        return this.organisationsList[o];
      }
    }
  }


  public makeTestRepo() {
    var response = new DBResponse<OrganisationDAO>();

    var org = new OrganisationDAO();
    var org2 = new OrganisationDAO();
    var org3 = new OrganisationDAO(); 
    var org4 = new OrganisationDAO();
    var org5 = new OrganisationDAO();

    org.setOrganisationID(1); org.setName("Organisation1"); org.setSector("Information Technology");
    org2.setOrganisationID(2); org2.setName("Organisation2"); org2.setSector("Information Technology");
    org3.setOrganisationID(3); org3.setName("Organisation3"); org3.setSector("Information Technology");
    org4.setOrganisationID(4); org4.setName("Organisation4"); org4.setSector("Information Technology");
    org5.setOrganisationID(5); org5.setName("Organisation5"); org5.setSector("Information Technology");
        
    response.rows = [org, org2, org3, org4, org5];
    
    this.updateOrganisationsList(response);
  }


  public subscribe(observer: Observer) {
    this.observers.push(observer);
  }

  public notify() {
    this.observers.forEach((observer)=>{
      observer.update();
    });
  }


  private makeDAOList(response){
    for(let o in response) {
      let organisation = new OrganisationDAO();

      response[o].id && organisation.setOrganisationID(response[o].id);
      response[o].name && organisation.setName(response[o].name);
      response[o].primaryContactId && organisation.setPrimaryContactId(response[o].primaryContactId);
      response[o].address1 && organisation.setAddress1(response[o].address1);
      response[o].address2 && organisation.setAddress2(response[o].address2);
      response[o].city && organisation.setCity(response[o].city);
      response[o].email && organisation.setEmail(response[o].email);
      response[o].phone && organisation.setPhone(response[o].phone);
      response[o].sector && organisation.setSector(response[o].sector);
      response[o].authKey && organisation.setAuthKey(response[o].authKey);

      this.organisationsList.push(organisation);
    }
  }

}
