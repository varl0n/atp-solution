import { TestBed } from '@angular/core/testing';

import { RepoOrganisations } from './repo-organisations.service';

describe('RepoOrganisations', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RepoOrganisations = TestBed.get(RepoOrganisations);
    expect(service).toBeTruthy();
  });
});
