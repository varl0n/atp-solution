import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManageOrganisationsComponent } from './view-manage-organisations.component';

describe('ViewManageOrganisationsComponent', () => {
  let component: ViewManageOrganisationsComponent;
  let fixture: ComponentFixture<ViewManageOrganisationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewManageOrganisationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManageOrganisationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
