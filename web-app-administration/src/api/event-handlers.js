const db = require('./database-helpers');
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
var env = require('./env.production');


module.exports = (function(){
    
    var connection = null;

    var EventHandler = function(){
        connection = new Connection(env.database);
    };



    /* 
     * Add new organisation then select all organisations from the DB and send them back
     */    
    EventHandler.prototype.addOrganisation = function(request, response, callback) {
        request.body.authKey = db.objectToMD5(request.body);
        let posted = db.objectPropToSQL(request.body);
        console.log('Add Organisation Request.');
        request = new Request(
            'INSERT INTO organisations (' + posted.columns + ') VALUES (' + posted.values + ');',
            function(err, rowCount, rows) {
                if (err) {
                    response.send({error: err, rows: []});
                }
            });

        request.on('error', function(err) {
            response.send({error: err, rows: []});
        })

        request.on('requestCompleted', function(){
            console.log('Selecting all results.');
            callback && callback(request, response);
        })

        // Execute SQL statement
        connection.execSql(request);
    };



    
    EventHandler.prototype.getOrganisations = function(req, response) {
        var query = "SELECT * FROM organisations WHERE status <> 0 ORDER BY id desc";
        var result = [];

            var request = new Request(query, function(err) {
                if (err) { 
                    response.send({error: err, rows: []});
                    return;
                }
            });

            request.on('row', function(columns) {
                var row = {};
                columns.forEach(function(column) {
                    row[column.metadata.colName] = column.value;
                });
                result.push(row);
            });

            request.on('doneProc', function(rowCount, more, rows) {
                if(!more){
                    response.send({error: false, rows: result});
                }
            });
            
            connection.execSql(request);
    };




    /* 
     * Add new organisation then select all organisations from the DB and send them back
     */    
    EventHandler.prototype.deleteOrganisation = function(request, response, callback) {
        
        console.log('Delete Organisation Request.', `DELETE FROM organisations WHERE id = ${request.body.id};`);
        request = new Request(
            `UPDATE organisations SET status = 0 WHERE id = ${request.body.id};`,
            function(err, rowCount, rows) {
                if (err) {
                    response.send({error: err, rows: []});
                }
            });

        request.on('error', function(err) {
            response.send({error: err, rows: []});
        });

        request.on('requestCompleted', function(){
            console.log('Selecting all results.');
            callback && callback(request, response);
        });

        // Execute SQL statement
        connection.execSql(request);
    };



    /* 
     * If the endpoint is unknown send back the error message
     */
    EventHandler.prototype.unknownEndpoint = function(req, res) {
        res.send(500, 'Please provide a valid endpoint for your request.');
    };



    return EventHandler;

})();